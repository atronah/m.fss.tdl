Проект интеграции [МИС Инфоклиника][sdsys] с сервисом [АИС ЭЛН][fss.eln]
==================================================================

**Версия**: (в разработке)

**Разработчик**: [ООО "МедИС Плюс"][mplus]

[TODO-список](TODO.md)

Описание
--------
Проект для реализации обмена данными о временной нетрудоспособности граждан между МИС Инфоклиника
и сервисом АИС ЭЛН от ФСС.


Техничечкие данные
------------------
- Протокол удаленного вызова процедур: SOAP 1.1 ([wsdl-описание][wsdl] АИС ЭЛН) поверх HTTP
    - Обеспечение целостности сообщений и[SOAP Message Security 1.1][wss1.1]
- Защита информации:
    - Шифрование данных по алгоритму *ГОСТ 28147-89*
    - ЭЦП по алгоритму ГОСТ Р 34.10-2001
        - Хеш-функция по алгоритму ГОСТ Р 34.11-94
        - Каноникализация по методу [C14N][c14n]
    - Проверка ЭЦП на стороне АИС ЭЛН: КриптоПро JCP


Документы
---------
- [Спецификация обмена](references/Спецификация_АИС_ЭЛН_МО_внешний.doc) - описание протокола обмена.
- [ГОСТ Р 34.10-2001 с cntd.ru](references/gost_r_34.10-2001.pdf) - текст ГОСТа по ЭЦП
- [ГОСТ Р 34.11-94 с cntd.ryu](references/gost_r_34.11-94.pdf) - текст ГОСТа по хеш-функции для ЭЦП
- [ГОСТ 28147-89 c gostexpert.ru](references/gost_28147-89.pdf)


Ссылки
------
- [Open-source реализации отечественных криптоГОСТов на Habr](https://habrahabr.ru/post/273055/)
- ЭЦП + хеш-функция:
    - Устаревшая, но используемая в АИС ЭЛН версия:
        - **ГОСТ Р 34.10-2001** (ЭЦП): [на gosthelp.ru](http://www.gosthelp.ru/gost/gost6784.html), он же [RFC4357][rfc4357]
        - **ГОСТ Р 34.11-94** (хеш-функция): [на gosthelp.ru](http://www.gosthelp.ru/gost/gost9658.html), он же [RFC5831][rfc5831]
    - Актуальная версия
        - **ГОСТ Р 34.10—2012** (ЭЦП): [на gost.ru](http://protect.gost.ru/document.aspx?control=7&id=180151)
        - **ГОСТ Р 34.11—2012** (хеш-функция): [на Викитеке](https://ru.wikisource.org/wiki/%D0%93%D0%9E%D0%A1%D0%A2_%D0%A0_34.11%E2%80%942012)
    - Описание Инфраструктуры общедоступных ключей X.509: [RFC2459][rfc2459]
    - Использование ЭЦП совместно с сертификатами X.509 PKI описано в [RFC4491][rfc4491]
    - Доп. алгоритмы шифрования для использования с обозначенным ГОСТами: [RFC4357][rfc4357]
- Шифрование данных:
    - **ГОСТ 28147-89** [на gost.ru](http://protect.gost.ru/v.aspx?control=7&id=139177)
- [Идентификаторы криптографических параметров алгоритмов](http://cpdn.cryptopro.ru/content/csp40/html/group___pro_c_s_p_ex_CP_PARAM_OIDS.html),
так же указаны в [RFC4357][rfc4357]
- [Пример программного использования OpenSSL Рутокен ЭЦП для подписи данных](http://forum.rutoken.ru/topic/1639/)


[mplus]: http://www.medis-plus.ru
[sdsys]: http://sdsys.ru/
[fss.eln]: https://portal.fss.ru/fss/regulations/bytheme/eln-pilot
[wsdl]: references/wsdl.xml
[wss1.1]: https://www.oasis-open.org/committees/download.php/16790/wss-v1.1-spec-os-SOAPMessageSecurity.pdf
[c14n]: https://www.w3.org/TR/xml-c14n
[rfc5831]: https://tools.ietf.org/html/rfc5831
[rfc4357]:https://tools.ietf.org/html/rfc4357
[rfc4491]:https://tools.ietf.org/html/rfc4491
[rfc4357]: https://tools.ietf.org/html/rfc4357
[rfc2459]: https://tools.ietf.org/html/rfc2459